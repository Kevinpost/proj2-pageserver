from flask import Flask, render_template, request, abort, send_from_directory

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/<name>')
def entry(name):

    file_extension_OK = False
    css_file = False
    if name.endswith(".html"):
        file_extension_OK = True
    if name.endswith(".css"):
        file_extension_OK = True
        css_file = True

    request_url = request.environ['REQUEST_URI']

    if ('~' in request_url) or ('..' in request_url) or ('//' in request_url) or (file_extension_OK == False):
        abort(403)
    else:
        if css_file:
            try:
                return send_from_directory("static/css/", name)
            except:
                abort(404)
        else:

            try:
                return render_template(name)
            except:
                abort(404)

@app.errorhandler(404)
def page_not_found(e):
    if ('//' in request.full_path):
        return render_template("403.html"), 403
    else:
        return render_template("404.html"), 404

@app.errorhandler(403)
def page_forbidden(e):
    return render_template("403.html"), 403


if __name__ == "__main__":
    app.run(debug=True,host = '0.0.0.0')
