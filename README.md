# README #

## **Author Information**

* Name: Kevin Post

* Contact address: Kpost7@uoregon.edu

## **Program description**

This is a basic web server written in Python using the Flask library and hosted in a docker container

App.py is the 'engine' of the server.  It checks for properly formatted URL requests, (if improperly formatted, returns 403 error).  It checks whether the file exists (if file does not exist, returns 404 error).  

If formatted properly and file exists, serves the page to the client.

## **Example usage**

To build the container:  docker build -t <containername> .
To run the container: docker run -d -p 5000:5000 <containername>

To stop the container: docker stop <dockerID>
